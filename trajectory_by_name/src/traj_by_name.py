#! /usr/bin/env python

import rospy
import rospkg
from trajectory_by_name_srv.srv import TrajByName, TrajByNameResponse
from iri_wam_reproduce_trajectory.srv import ExecTraj, ExecTrajRequest

def my_callback(request):
    rospy.wait_for_service('/execute_trajectory')
    execute_trajectory_service_client = rospy.ServiceProxy('/execute_trajectory', ExecTraj) # Create the connection to the service
    execute_trajectory_request_object = ExecTrajRequest()
    rospack = rospkg.RosPack()
    trajectory_file_path = rospack.get_path('iri_wam_reproduce_trajectory') + "/config/" + request.traj_name + ".txt"
    execute_trajectory_request_object.file = trajectory_file_path # Fill the variable file of this object with the desired file path
    result = execute_trajectory_service_client(execute_trajectory_request_object)
    response = TrajByNameResponse()
    response.success = True
    response.status_message = "Successfully executed trajectory"
    return response

rospy.init_node('traj_by_name_node')
my_service = rospy.Service('/trajectory_by_name', TrajByName , my_callback) # create the Service called my_service with the defined callback
rospy.spin() # maintain the service open.